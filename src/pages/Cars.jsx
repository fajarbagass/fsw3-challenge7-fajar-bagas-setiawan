import { Navbar, FilterCar, Footer } from "../components";
import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { getCars, carsSelectors } from '../features/carsSlice';
import { Container, Row, Col, Card, Button } from 'react-bootstrap';
import "../components/Cars.css";

const Cars = () => {
    const dispath = useDispatch();
    const cars = useSelector(carsSelectors.selectAll);

    useEffect(() => {
        dispath(getCars());
    }, [dispath]);

    const imgUrl = "https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/public";

    return (
        <div>
            <Navbar />
            <Container fluid className="pt-5" style={{backgroundColor: "#F1F3FF"}}>
                <Row className="pt-5">
                    <Col md className="pt-5" style={{marginLeft: "94px"}}>
                        <h1 claasName="font-title">Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)</h1>
                        <p className="font-body pe-5 me-5">Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</p>
                    </Col>
                    <Col md className="pe-0">
                        <img src={process.env.PUBLIC_URL + '/img/img_car.png'} style={{ width: "663px",height: "374.05px"}} />
                    </Col>
                </Row>
            </Container>            
            <FilterCar />
            <Container className="d-flex flex-row flex-wrap py-5 px-auto">
                {cars.map((cars) => (
                <Card className="shadow p-3" style={{ width: '333px', height: '586px', marginRight: "24px", marginBottom: "24px"}} key={cars.id}>
                    <div className="m-auto pt-3">
                        <Card.Img variant="top" src={imgUrl + cars.image.substring(1)} style={{width: "270px",height: "160px"}} />
                    </div>
                    <Card.Body>
                        <Card.Text><h6>{cars.manufacture} {cars.model}</h6></Card.Text>
                        <Card.Title>Rp {cars.rentPerDay} / hari</Card.Title>
                        <Card.Text>{cars.description}</Card.Text>
                        <Card.Text><img src={process.env.PUBLIC_URL + '/img/fi_users.svg'} /> {cars.capacity} Orang</Card.Text>
                        <Card.Text><img src={process.env.PUBLIC_URL + '/img/fi_settings.svg'} /> {cars.transmission}</Card.Text>
                        <Card.Text><img src={process.env.PUBLIC_URL + '/img/fi_calendar.svg'} /> Tahun {cars.year}</Card.Text>
                    </Card.Body>
                    <Button className="w-100" variant="success">Pilih Mobil</Button>
                </Card>
                ))}
            </Container>
            <Footer />
        </div>
    )
}

export default Cars;