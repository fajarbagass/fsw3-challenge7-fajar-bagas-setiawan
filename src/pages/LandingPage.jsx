import {
    Navbar,
    Main,
    OurService,
    WhyUs,
    Testimonial,
    Banner,
    FAQ,
    Footer
} from '../components';


function LandingPage () {
    return (
        <div>
            <Navbar />
            <Main />
            <OurService />
            <WhyUs />
            <Testimonial />
            <Banner />
            <FAQ />
            <Footer />
        </div>
    )
}

export default LandingPage;