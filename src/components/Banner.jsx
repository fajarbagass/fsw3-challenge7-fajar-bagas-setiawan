import { Container, Button } from 'react-bootstrap';
import "./Cars.css";

function Banner() {
    return (
        <Container id="banner" style={{ background: "#0D28A6", color: "#FFFFFF"}} className="my-5 p-5 rounded-3">
            <Container className="text-center p-5">
                <h1 className="font-title">Sewa Mobil di (Lokasimu) Sekarang</h1>
                <p className="font-body">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                <Button href="/cars" variant="success">Mulai Sewa Mobil</Button>
            </Container>
        </Container>
    )
}

export default Banner;