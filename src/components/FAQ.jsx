import { Container, Row, Col, Accordion, Stack } from "react-bootstrap";
import "./Cars.css";

function FAQ() {
    return (
        <Container id="faq" className="px-0 py-5">
            <Row className="pt-3">
                <Col lg>
                    <h3 className="font-header">Frequently Asked Question</h3>
                    <p className="font-body">Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                </Col>
                <Col lg>
                    <Accordion flush className="font-body">
                        <Stack direction="vertical" gap={3}>
                            <Accordion.Item eventKey="0">
                                <Accordion.Header>Apa saja syarat yang dibutuhkan?</Accordion.Header>
                                <Accordion.Body>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum cum optio hic atque enim, tempora illo consequuntur neque fugit tempore unde a voluptatem nobis ipsum dignissimos voluptates ea itaque. Quasi!
                                </Accordion.Body>
                            </Accordion.Item>
                            <Accordion.Item eventKey="1">
                                <Accordion.Header>Berapa hari minimal sewa mobil lepas kunci?</Accordion.Header>
                                <Accordion.Body>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum cum optio hic atque enim, tempora illo consequuntur neque fugit tempore unde a voluptatem nobis ipsum dignissimos voluptates ea itaque. Quasi!
                                </Accordion.Body>
                            </Accordion.Item>
                            <Accordion.Item eventKey="2">
                                <Accordion.Header>Berapa hari sebelumnya sabaiknya booking sewa mobil?</Accordion.Header>
                                <Accordion.Body>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum cum optio hic atque enim, tempora illo consequuntur neque fugit tempore unde a voluptatem nobis ipsum dignissimos voluptates ea itaque. Quasi!
                                </Accordion.Body>
                            </Accordion.Item>
                            <Accordion.Item eventKey="3">
                                <Accordion.Header>Apakah Ada biaya antar-jemput?</Accordion.Header>
                                <Accordion.Body>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum cum optio hic atque enim, tempora illo consequuntur neque fugit tempore unde a voluptatem nobis ipsum dignissimos voluptates ea itaque. Quasi!
                                </Accordion.Body>
                            </Accordion.Item>
                            <Accordion.Item eventKey="4">
                                <Accordion.Header>Bagaimana jika terjadi kecelakaan</Accordion.Header>
                                <Accordion.Body>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum cum optio hic atque enim, tempora illo consequuntur neque fugit tempore unde a voluptatem nobis ipsum dignissimos voluptates ea itaque. Quasi!
                                </Accordion.Body>
                            </Accordion.Item>
                        </Stack>
                    </Accordion>
                </Col>
            </Row>
        </Container>
    )
}
export default FAQ;