import { Container, Row, Col, ListGroup } from 'react-bootstrap';
import "./Cars.css";

function OurService() {
    return (
        <Container id="ourService" className="py-5">
            <Row className="pt-4">
                <Col lg><img src={process.env.PUBLIC_URL + '/img/img_service.png'} /></Col>
                <Col lg>
                    <h3 className="font-header">Best Car Rental for any kind of trip in (lokasimu)!</h3>
                    <p className="font-body">Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga lebih murah dibandingkan yang lain, kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.</p>
                    <ListGroup>
                        <ListGroup.Item className="border-0 ps-0">
                            <img src={process.env.PUBLIC_URL + '/img/icon_list.svg'} />
                            <label className="ms-3 font-body">Sewa Mobil Dengan Supir di Bali 12 Jam</label>
                        </ListGroup.Item>
                        <ListGroup.Item className="border-0 ps-0">
                            <img src={process.env.PUBLIC_URL + '/img/icon_list.svg'} />
                            <label className="ms-3 font-body">Sewa Mobil Lepas Kunci di Bali 12 Jam</label>
                        </ListGroup.Item>
                        <ListGroup.Item className="border-0 ps-0">
                            <img src={process.env.PUBLIC_URL + '/img/icon_list.svg'} />
                            <label className="ms-3 font-body">Sewa Mobil Jangka Panjang Bulanan</label>
                        </ListGroup.Item>
                        <ListGroup.Item className="border-0 ps-0">
                            <img src={process.env.PUBLIC_URL + '/img/icon_list.svg'} />
                            <label className="ms-3 font-body">Layanan Aiport Transfer / Drop In Out</label>
                        </ListGroup.Item>
                        <ListGroup.Item className="border-0 ps-0">
                            <img src={process.env.PUBLIC_URL + '/img/icon_list.svg'} />
                            <label className="ms-3 font-body">Sewa Mobil Dengan Supir di Bali 12 Jam</label>
                        </ListGroup.Item>
                    </ListGroup>
                </Col>
            </Row>
        </Container>
    )
}

export default OurService;