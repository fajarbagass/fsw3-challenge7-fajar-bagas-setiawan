import { Container, Form, Row, Col, Button } from 'react-bootstrap';
import "./Cars.css";
import { useState, useEffect } from 'react';
function FilterCar() {

    return (
        <Container className="shadow p-4 bg-body filter-container rounded-3">
            <Row>
                <Col md={3}>
                    <p>Tipe Driver</p>
                    <Form.Select name="driver">
                        <option selected hidden>Pilih Tipe Driver</option>
                        <option value="withDriver">Dengan Sopir</option>
                        <option value="withoutDriver">Tanpa Sopir (Lepas Kunci)</option>
                    </Form.Select>
                </Col>
                <Col>
                    <p>Tanggal</p>
                    <Form.Control type="date" placeholder="Pilih Tanggal" name="date" />
                </Col>
                <Col>
                    <p>Waktu Jemput/Ambil</p>
                    <Form.Control type="time" placeholder="Pilih Waktu" name="time" />
                </Col>
                <Col md={3}>
                    <p>Jumlah Penumpang (optional)</p>
                    <Form.Control type="number" placeholder="Jumlah Penumpang" name="capacity" min="0" />
                </Col>
                <Col md={2} className="mt-auto">
                    <Button variant="success">
                        Cari Mobil
                    </Button>
                </Col>
            </Row>
        </Container>
    )
}

export default FilterCar;