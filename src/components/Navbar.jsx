import { Container, Navbar, Nav, Stack, Button } from 'react-bootstrap';
import './Cars.css';

function navbar () {
    return (
            <Navbar className="pt-3" bg="tranparent" expand="md" fixed="top">
                <Container className="ps-0">
                    <Navbar.Brand><img src={process.env.PUBLIC_URL + '/img/icon.jpeg'} /></Navbar.Brand>
                    <Nav className="ms-auto font-navbar">
                        <Stack direction="horizontal" gap={3}>
                            <Nav.Link href="#ourService">Our Service</Nav.Link>
                            <Nav.Link href="#whyUs">Why Us</Nav.Link>
                            <Nav.Link href="#testimonial">Testimonial</Nav.Link>
                            <Nav.Link href="#faq">FAQ</Nav.Link>
                            <Button className="font-button" variant="success">Register</Button>
                        </Stack>
                    </Nav>
                </Container>
            </Navbar>
    )
}

export default navbar;