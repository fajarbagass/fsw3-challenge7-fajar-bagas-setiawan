import "./Cars.css";
import { Container, Card, Stack } from 'react-bootstrap';
import "./Cars.css";

function WhyUs() {
    return (
        <Container id="whyUs" className="pt-5 px-0">
            <h3 className="font-header mt-3">Why Us?</h3>
            <p className="font-body">Mengapa harus pilih Binar Car Rental?</p>
            <Container className="py-4 px-0">
                <Stack  className="d-flex flex-row flex-wrap" direction="horizontal" gap={3}>
                    <Card style={{ width: "268px", height: "196px" }}>
                        <Card.Body className="p-4">
                            <Card.Title><img src={process.env.PUBLIC_URL + '/img/icon_complete.svg'} /></Card.Title>
                            <Card.Title>Mobil Lengkap</Card.Title>
                            <Card.Text className="font-body">
                            Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <Card style={{ width: "268px", height: "196px" }}>
                        <Card.Body className="p-4">
                            <Card.Title><img src={process.env.PUBLIC_URL + '/img/icon_price.svg'} /></Card.Title>
                            <Card.Title>Harga Murah</Card.Title>
                            <Card.Text className="font-body">
                            Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil lain                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <Card style={{ width: "268px", height: "196px" }}>
                        <Card.Body className="p-4">
                            <Card.Title><img src={process.env.PUBLIC_URL + '/img/icon_24hrs.svg'} /></Card.Title>
                            <Card.Title>Layanan 24 Jam</Card.Title>
                            <Card.Text className="font-body">
                            Siap melayani kebutuhan Anda selama 24 jam nonstop. kami juga tersedia di akhir minggu
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <Card style={{ width: "268px", height: "196px" }}>
                        <Card.Body className="p-4">
                            <Card.Title><img src={process.env.PUBLIC_URL + '/img/icon_professional.svg'} /></Card.Title>
                            <Card.Title>Sopir Profesional</Card.Title>
                            <Card.Text className="font-body">
                            Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Stack>
            </Container>
        </Container>
    )
}

export default WhyUs;