import { Container, Row, Col, ListGroup, Stack } from "react-bootstrap";
import "./Cars.css";

function Footer() {
    return (
        <Container id="footer" className="py-5">
            <Row>
                <Col md>
                    <ListGroup className="font-body">
                        <ListGroup.Item className="border-0 ps-0">
                            Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000
                        </ListGroup.Item>
                        <ListGroup.Item className="border-0 ps-0">
                            binarcarrental@gmail.com
                        </ListGroup.Item>
                        <ListGroup.Item className="border-0 ps-0">
                            081-233-334-808
                        </ListGroup.Item>
                    </ListGroup>
                </Col>
                <Col md>
                    <ListGroup.Item className="border-0 ps-0">
                       <a href="#ourService" className="text-decoration-none text-black"><h6>Our Service</h6></a>
                    </ListGroup.Item>
                    <ListGroup.Item className="border-0 ps-0">
                       <a href="#whyUs" className="text-decoration-none text-black"><h6>WhyUs</h6></a>
                    </ListGroup.Item>
                    <ListGroup.Item className="border-0 ps-0">
                       <a href="#testimonial" className="text-decoration-none text-black"><h6>Testimonial</h6></a>
                    </ListGroup.Item>
                    <ListGroup.Item className="border-0 ps-0">
                       <a href="#faq" className="text-decoration-none text-black"><h6>FAQ</h6></a>
                    </ListGroup.Item>
                </Col>
                <Col md>
                    <p className="font-body">Connect with us</p>
                    <ListGroup>
                        <ListGroup.Item className="border-0 ps-0">
                            <Stack direction="horizontal" gap={3}>
                                <a href="#facebook">
                                    <img src={process.env.PUBLIC_URL + '/img/icon_facebook.svg'} />
                                </a>
                                <a href="#instagram">
                                    <img src={process.env.PUBLIC_URL + '/img/icon_instagram.svg'} />
                                </a>
                                <a href="#twitter">
                                    <img src={process.env.PUBLIC_URL + '/img/icon_twitter.svg'} />
                                </a>
                                <a href="#mail">
                                    <img src={process.env.PUBLIC_URL + '/img/icon_mail.svg'} />
                                </a>
                                <a href="#twitch">
                                    <img src={process.env.PUBLIC_URL + '/img/icon_twitch.svg'} />
                                </a>
                            </Stack>
                        </ListGroup.Item>
                    </ListGroup>
                </Col>
                <Col md>
                    <p className="font-body">Copyright Binar 2022</p>
                    <img src={process.env.PUBLIC_URL + '/img/icon.jpeg'} />
                </Col>
            </Row>
        </Container>
    )
}

export default Footer;