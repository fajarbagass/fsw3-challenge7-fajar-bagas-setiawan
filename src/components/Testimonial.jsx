import { Container, Carousel, Card } from 'react-bootstrap';
import "./Cars.css";

function Testimonial() {
    return (
        <Container id="testimonial" className="d-flex flex-column align-items-center py-5">
            <Container className="text-center py-4">
                <h3 className="font-header">Testimonial</h3>
                <p className="font-body">Berbagai review positif dari para pelanggan kami</p>
            </Container>
            <Carousel>
                <Carousel.Item>
                    <Card className="d-flex flex-row border-0" style={{ width: "619px",height: "270px", backgroundColor: "#F1F3FF" }}>
                        <Card.Body className="d-flex align-content-center mx-4">
                            <img src={process.env.PUBLIC_URL + '/img/img_photo(1).png'} style={{width: "80px",height: "80px"}} className="align-self m-auto" />
                        </Card.Body>
                        <Card.Body className="pt-5 me-3">
                            <Card.Title><img src={process.env.PUBLIC_URL + '/img/rate.svg'} /></Card.Title>
                            <Card.Text className="font-body">
                            “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”
                            </Card.Text>
                            <Card.Title>John Dee 32, Bromo</Card.Title>
                        </Card.Body>
                    </Card>
                </Carousel.Item>
                <Carousel.Item>
                    <Card className="d-flex flex-row border-0" style={{ width: "619px",height: "270px", backgroundColor: "#F1F3FF" }}>
                        <Card.Body className="d-flex align-content-center mx-4">
                            <img src={process.env.PUBLIC_URL + '/img/img_photo(2).png'} style={{width: "80px",height: "80px"}} className="align-self m-auto" />
                        </Card.Body>
                        <Card.Body className="pt-5 me-3">
                            <Card.Title><img src={process.env.PUBLIC_URL + '/img/rate.svg'} /></Card.Title>
                            <Card.Text className="font-body">
                            “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”
                            </Card.Text>
                            <Card.Title>John Dee 32, Bromo</Card.Title>
                        </Card.Body>
                    </Card>
                </Carousel.Item>
            </Carousel>
        </Container>
    )
}

export default Testimonial;