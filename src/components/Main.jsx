import { Container, Row, Col, Button } from 'react-bootstrap';
import "./Cars.css";

function Main() {
    return (
        <Container fluid className="pt-5" style={{backgroundColor: "#F1F3FF"}}>
            <Row className="pt-5">
                <Col md className="pt-5" style={{marginLeft: "94px"}}>
                    <h1 claasName="font-title">Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)</h1>
                    <p className="font-body pe-5 me-5">Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</p>
                    <Button href="cars" variant="success" className="font-button">Mulai Sewa Mobil</Button>
                </Col>
                <Col md className="pe-0">
                    <img src={process.env.PUBLIC_URL + '/img/img_car.png'} style={{ width: "663px",height: "374.05px"}} />
                </Col>
            </Row>
        </Container>
    )
}

export default Main;