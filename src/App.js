import { LandingPage, Cars } from './pages';
import { BrowserRouter, Routes, Route } from 'react-router-dom';


function App() {
  return (
    <BrowserRouter>
      <Routes>
          <Route path="/" element={<LandingPage />} />
          <Route path="cars" element={<Cars />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
